// 1) Comente o que faz cada linha do programa.
// 2) a.personagens = new Personagem[3]; descubra o que faz exatamente esta linha.
// 3) Da forma como está, quantos personagens cabem em um seriado? faca com que seja possível colocar até 10 personagens
// 4) O que acontece se colocarmos as linhas 23-28 ante da linha 14? Descreva, com suas palavras o problema.
// 5) Faca com que Daenerys seja o primeiro nome a aparecer na tela.
// 6) Adicione ano de lancamento ao seriado.
// 7) Crie mais um seriado a sua escolha.
// 8) Faca com que uma personagem tenha sua atriz ou ator e este tenha nome e sobrenome.
// 9) Mostre na tela infos dos seriados cadastrados.

class Main {
  public static void main(String[] args) {

      Personagem p = new Personagem();
      p.nome = "Tyrion Lannister";

      Personagem z = new Personagem();
      z.nome = "Daenerys Targaryen";

      Personagem e = new Personagem();
      e.nome = "Arya Stark";

      Seriado a = new Seriado();
      a.titulo = "Game of Thrones";
      a.personagens = new Personagem[3];
      a.personagens[0] = p;
      a.personagens[1] = z;
      a.personagens[2] = e;

      System.out.println(a.titulo);
      for(int i =0;i<a.personagens.length;i++){
        System.out.println("- "+a.personagens[i].nome);
      }
  }
}

class Seriado{
  String titulo;
  Personagem[] personagens;
}

class Personagem{
  String nome;
}
